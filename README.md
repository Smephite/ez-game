# "eZ Game"
"eZ Game" is my first Java Game.
 
It's an space game in which you need to collect coins (more to come!).

I started on 05.13.15


* 05.13.15
    * Added basic functionality
    * Added nice framework to handle multiple entitys
    * Added moving background
    * Added relative speed to the position (more left -> faster background and other entity movement, slower player => looks faster
    * Added basic Sound
    * Added support for multiple Screen options (fullscreen, borderless, ...)
    * Added basic FPS display
    * Added Coins
    * Added Textures
    * Added Bullets/Shots
    * Added nice framework to handle Keys
* 05.14.15
    * Improved Sound System
    * Improved display of FPS
    * Added display of points
    * Added "Lobby" / GameOver screen
    * Improved / Added new way to handle FPS (now dedicaded from TPS)
    
    
##TODO
 * Add enemys
 * Save score
 * Add highscore screen
 * Add (Xbox)controller support
 * Fix background music
 * Add better soundeffects
 * Add better graphics
 * make it more shiney