package me.mchacker1706.game;

import lombok.Getter;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;

public class AnimationHelper {

    private @Getter ArrayList<BufferedImage> images = new ArrayList<>();
    private @Getter int index = 0;


    public AnimationHelper(BufferedImage... images)
    {

        this.images = (ArrayList<BufferedImage>)Arrays.asList((BufferedImage[])images);


    }

    public AnimationHelper(String... resources)
    {

        for(String s : resources){
            try{
                images.add(ImageIO.read(getClass().getClassLoader().getResourceAsStream(s)));
            }catch (Exception e){
                e.printStackTrace();
            }
        }


    }


    public void next(){
        index++;
        if(index>=images.size())
            index = 0;
    }


    public BufferedImage getCurrent(){
        return images.get(index);
    }



    public void reset(){
        index = 0;
    }













}
