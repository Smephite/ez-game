package me.mchacker1706.game;

import lombok.Getter;
import lombok.Setter;
import me.mchacker1706.game.ai.Spawning;
import me.mchacker1706.game.entity.Entity;
import me.mchacker1706.game.entity.EntityEnemy;
import me.mchacker1706.game.entity.EntityPlayer;
import me.mchacker1706.game.enums.Key;
import me.mchacker1706.game.properties.ScreenProperties;

import java.awt.*;
import java.util.ArrayList;

import static me.mchacker1706.game.properties.FinalProperties.*;
public class MainThread extends Thread{

    private @Getter static MainThread instance;
    private @Getter static MainFrame mainFrame = new MainFrame();
    private @Getter static KeyHandler keyHandler = new KeyHandler();
    private @Getter static EntityPlayer entityPlayer;
    private @Getter static Rectangle outerBorder = new Rectangle(0, 0, ScreenProperties.width, ScreenProperties.height);
    private @Getter static Background background = new Background();
    private @Getter static int points = 0;
    private @Getter @Setter static EntityEnemy enemy;
    private @Getter static long tick;

    private @Getter static boolean hitboxActive = false;
    private @Getter static boolean fpsActive = false;
    private @Getter static int coins = 0;

    private boolean running = false;

    public static void addPoint(){
        points++;
    }
    public static void addPoint(int value){
        points+=value;
    }
    public static void addCoin(){
        addPoint();
        coins++;
    }

    public MainThread(){

        instance = this;

        mainFrame.setName(windowName);

        mainFrame.addKeyListener(new KeyHandler());

        entityPlayer = new EntityPlayer();
        entityPlayer.getLocation().setPosY(ScreenProperties.height/2);

    }

    @Override
    public synchronized void start() {
        super.start();



        running = true;
    }

    private long startedTick = 0;
    private long lastFPSTime = 0;
    @Getter
    private static int lastFPS = 0;
    private int tmpFPS = 0;

    private long lastTPSTime = 0;
    @Getter
    private static int lastTPS = 0;
    private int tmpTPS = 0;



    private boolean lastFPSButton = false;
    private long lastFPSButtonTime = 0;

    private boolean lastHBoxButton = false;
    private long lastHBoxButtonTime = 0;

    private boolean first = true;

    private long lastSpace = 0;
    @Override
    public void run() {


        new Thread(()->{

            while (true)
            {

                if(System.currentTimeMillis()-lastFPSTime>=1000){
                    lastFPSTime = System.currentTimeMillis();
                    lastFPS = (tmpFPS);
                    tmpFPS = 0;
                }
                if(System.currentTimeMillis()-lastTPSTime>=1000){
                    lastTPSTime = System.currentTimeMillis();
                    lastTPS = (tmpTPS);
                    tmpTPS = 0;
                }


            }


        }).start();


        new Thread(()->{

            while(true) {
                tmpFPS++;
                mainFrame.tick();

                try {
                    if(ScreenProperties.FPSLimit!=-1)
                    sleep(1000/ScreenProperties.FPSLimit, (int)(1000.0/ScreenProperties.FPSLimit-((int)(1000.0/ScreenProperties.FPSLimit)))*1000);

                } catch (Exception e) {
                }
            }

        }).start();

        while(isRunning()) {
            startedTick = System.currentTimeMillis();
            tmpTPS++;
            tick++;

            if(KeyHandler.getInstance().isKeyPressed(Key.KEY_FPS) && !lastFPSButton && System.currentTimeMillis()-lastFPSButtonTime>=250)
            {

                lastFPSButtonTime = System.currentTimeMillis();
                lastFPSButton = true;
                fpsActive=!fpsActive;

            }else
                lastFPSButton = false;

            if(KeyHandler.getInstance().isKeyPressed(Key.KEY_HBOX) && !lastHBoxButton && System.currentTimeMillis()-lastHBoxButtonTime>=250)
            {

                lastHBoxButtonTime = System.currentTimeMillis();
                lastFPSButton = true;
                hitboxActive=!hitboxActive;

            }else
                lastFPSButton = false;


            if(KeyHandler.getInstance().isKeyPressed(Key.KEY_SHOOT) && (GameState.getState().equals(GameState.LOBBY)||GameState.getState().equals(GameState.GAME_OVER)))
            {



                if(GameState.getState().equals(GameState.LOBBY)) {
                    GameState.setState(GameState.INGAME);
                    Spawning.setLastSpawn(System.currentTimeMillis() + 500);
                    entityPlayer.setNextShoot(tick + 30);

                }else{

                    KeyHandler.getPressedKeys().put(Key.KEY_SHOOT, false);
                    GameState.setState(GameState.LOBBY);
                    points = 0;
                    Entity.getEntities().clear();
                    entityPlayer = new EntityPlayer();
                }
            }

            if(KeyHandler.getInstance().isKeyPressed(Key.KEY_HAX) && KeyHandler.getInstance().isKeyPressed(Key.KEY_BOSS) && GameState.getState().equals(GameState.INGAME) && enemy==null)
                enemy = new EntityEnemy();



            for(Entity e : ((ArrayList<Entity>)Entity.getEntities().clone()))
                e.handle();


            background.tick();

            Spawning.handleEnemySpawn();
            Spawning.handleItemSpawn();



            if(keyHandler.isKeyPressed(Key.KEY_EXIT))
            {

                KeyHandler.getPressedKeys().put(Key.KEY_EXIT, false);
                if(GameState.getState().equals(GameState.LOBBY) ||GameState.getState().equals(GameState.GAME_OVER))
                {
                    break;
                }else{
                    entityPlayer.onDeath();
                }

            }
            sleepTick();
        }

        mainFrame.dispose();

        SoundUtils.clear();

        System.exit(0);

    }



    private long behind = 0;

    private void sleepTick(){

        long current = System.currentTimeMillis();

        if(startedTick-current>(1000/ticksPerSecond))
            behind = startedTick - current;

        try {

            if(behind>0){
                System.out.println(behind+" milliseconds behind! Skipping...");

                if(behind<1000 / ticksPerSecond)
                    sleep(1000 / ticksPerSecond - behind);

                behind-= 1000 / ticksPerSecond;
            }else{
                sleep(1000/ticksPerSecond);
            }


        } catch (Exception e) {
        //TODO
        }
    }


    public boolean isRunning(){

        return running;

    }

    public void exit(){
        running = false;
    }

}
