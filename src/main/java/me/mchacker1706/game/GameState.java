package me.mchacker1706.game;

import lombok.Getter;
import lombok.Setter;

public enum  GameState {

    LOBBY,
    INGAME,
    GAME_OVER;

    @Getter @Setter
    private static GameState state = LOBBY;
}
