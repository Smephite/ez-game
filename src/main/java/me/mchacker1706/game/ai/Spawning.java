package me.mchacker1706.game.ai;


import lombok.Getter;
import lombok.Setter;
import me.mchacker1706.game.GameState;
import me.mchacker1706.game.MainThread;
import me.mchacker1706.game.entity.EntityEnemy;
import me.mchacker1706.game.entity.EntityItem;
import me.mchacker1706.game.properties.ScreenProperties;

import java.util.Random;

public class Spawning {

    @Getter @Setter
    private static int spawnRate = 1; //per second

    @Setter @Getter
    private static long lastSpawn = 0;


    private static Random random = new Random();
    public static void handleItemSpawn(){

        if(!GameState.getState().equals(GameState.INGAME))
            return;

        if(MainThread.getEnemy()!=null)
            return;

        if(System.currentTimeMillis()-lastSpawn>=1000/spawnRate){
            lastSpawn = System.currentTimeMillis();

            int y = random.nextInt(ScreenProperties.height- MainThread.getMainFrame().getInsets().top);

            new EntityItem().getLocation().setPosY(y);


        }



    }

    private static int lastCoins = 0;

    public static void handleEnemySpawn() {


        if(MainThread.getCoins()-lastCoins==25 && MainThread.getEnemy()==null) {

            lastCoins+=25;
            MainThread.setEnemy(new EntityEnemy());
        }





    }
}
