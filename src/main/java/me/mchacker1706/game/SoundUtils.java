package me.mchacker1706.game;

import javafx.embed.swing.JFXPanel;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.*;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class SoundUtils {

    private static HashMap<String, File> fileHashMap = new HashMap<>();
    private static HashMap<String, Media> players = new HashMap<>();


    public static void clear(){

        for(Map.Entry<String, File> f : fileHashMap.entrySet())
            f.getValue().delete();


    }

    public static void nothing(){}

    static {




        try {
            fileHashMap.put("coin", File.createTempFile("coin", ".mp3"));
            fileHashMap.put("tetris", File.createTempFile("tetris", ".mp3"));
            fileHashMap.put("laser", File.createTempFile("laser_sound", ".mp3"));

            copyISToFile(SoundUtils.class.getClassLoader().getResourceAsStream("sound/coin.mp3"), fileHashMap.get("coin"));
            copyISToFile(SoundUtils.class.getClassLoader().getResourceAsStream("sound/tetris.mp3"), fileHashMap.get("tetris"));
            copyISToFile(SoundUtils.class.getClassLoader().getResourceAsStream("sound/laser_sound.mp3"), fileHashMap.get("laser"));

            new JFXPanel();
            players.put("coin", new Media(fileHashMap.get("coin").toURI().toString()));
            players.put("tetris", new Media(fileHashMap.get("tetris").toURI().toString()));
            players.put("laser", new Media(fileHashMap.get("laser").toURI().toString()));


        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }


    public static void playSound(String name){
        playSound(name, false);

    }

    public static void playSound(String name, boolean auto)
    {

        try {


            MediaPlayer pl = new MediaPlayer(players.get(name.toLowerCase()));

            if(auto)
                pl.setCycleCount(Integer.MAX_VALUE);

            pl.play();
           // players.put(name.toLowerCase(), new Media(fileHashMap.get(name.toLowerCase()).toURI().toString()));

        }catch (Exception e){

            e.printStackTrace();
            try{


                Media m = new Media(fileHashMap.get(name.toLowerCase()).toURI().toString());

                MediaPlayer me = new MediaPlayer(m);

                if(auto)
                    me.setCycleCount(Integer.MAX_VALUE);

                me.play();

                players.put(name.toLowerCase(), new Media(fileHashMap.get(name.toLowerCase()).toURI().toString()));

            }catch (Exception ee){

                ee.printStackTrace();


            }

        }

    }



    public static void copyISToFile(InputStream stream, File output){

        try {

            OutputStream outStream = new FileOutputStream(output);

            byte[] buffer = new byte[8 * 1024];
            int bytesRead;

            while ((bytesRead = stream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }
            stream.close();
            outStream.close();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

}
