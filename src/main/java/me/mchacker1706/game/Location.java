package me.mchacker1706.game;

import lombok.Getter;

public class Location {

    private @Getter float posX, posY;



    public Location(float posX, float posY)
    {
        this.posX = posX;
        this.posY = posY;
    }


    public float distance(Location location)
    {

        return (float)Math.sqrt((location.getPosX()-getPosX())*(location.getPosX()-getPosX())+(location.getPosY()-getPosY())*(location.getPosY()-getPosY()));

    }

    public float distanceX(Location location)
    {

        return (float)Math.sqrt((location.getPosX()-getPosX())*(location.getPosX()-getPosX()));

    }

    public float distanceY(Location location)
    {

        return (float)Math.sqrt((location.getPosY()-getPosY())*(location.getPosY()-getPosY()));

    }

    public Location clone(){
        return new Location(posX, posY);
    }

    public Location addX(float x){
        posX += x;
        return this;
    }


    public Location addY(float y){
        posY += y;
        return this;
    }


    public Location add(float x, float y){
        posX += x;
        posY += y;
        return this;
    }

    public Location set(float x, float y){
        this.posX = x;
        this.posY = y;
        return this;
    }

    public Location setPosX(float x){
        this.posX = x;
        return this;
    }

    public Location setPosY(float y){
        this.posY = y;
        return this;
    }

}
