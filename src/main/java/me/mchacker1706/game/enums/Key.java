package me.mchacker1706.game.enums;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static java.awt.event.KeyEvent.*;

public enum Key {


    KEY_UP(VK_W, VK_UP),
    KEY_DOWN(VK_S, VK_DOWN),
    KEY_RIGHT(VK_D, VK_RIGHT),
    KEY_LEFT(VK_A, VK_LEFT),
    KEY_EXIT(VK_ESCAPE),
    KEY_SHOOT(VK_SPACE),
    KEY_FPS(VK_F),
    KEY_HBOX(VK_H),
    KEY_BOSS(VK_B),
    KEY_HAX(VK_CONTROL),
    UNDEFINED(-1);

    @Getter
    private ArrayList<Integer> event;

    Key(int... event)
    {

        this.event = new ArrayList();

        for(int i : event)
            this.event.add(i);
    }


    private static HashMap<Integer, Key> list = new HashMap<>();


    public static Key byEvent(int event){

        Key k = list.get(event);

        if(k == null)
            k = UNDEFINED;

        return k;
    }

    static {

        for(Key k : Key.values())
        {
            for(int i : k.event)
                list.put(i, k);

        }

    }

}
