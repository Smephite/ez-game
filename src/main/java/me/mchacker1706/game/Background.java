package me.mchacker1706.game;


import lombok.Getter;
import lombok.Setter;
import me.mchacker1706.game.properties.ScreenProperties;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;

public class Background {

    private float x = 0;
    @Getter @Setter
    private float speed = 3;
    private BufferedImage texture;

    private int heigth, width;

    public Background(){

        try {
            texture = ImageIO.read(getClass().getClassLoader().getResourceAsStream("gfx/weltraum.png"));
        }catch (Exception e){
            e.printStackTrace();
        }

        heigth = texture.getHeight()* ScreenProperties.screenModifier;
        width = texture.getWidth()*ScreenProperties.screenModifier;

    }

    public void tick()
    {


        x-=speed;

        if(x<-texture.getWidth())
            x=0;


    }

    public void paint(Graphics g){


        g.drawImage(texture, (int)x, 0, width, heigth, null);

        g.drawImage(texture, (int) x + texture.getWidth(), 0, width, heigth, null);



    }




}
