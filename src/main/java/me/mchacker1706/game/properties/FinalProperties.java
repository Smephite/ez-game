package me.mchacker1706.game.properties;


public class FinalProperties {


    public static final int ticksPerSecond = 100;
    public static final String windowName = "eZ Game";
    public static DisplayMode displayMode = DisplayMode.BORDERLESS;


    public enum DisplayMode{

        NORMAL,
        FULLSCREEN,
        FULLSCREEN_BORDERLESS,
        BORDERLESS

    }

}
