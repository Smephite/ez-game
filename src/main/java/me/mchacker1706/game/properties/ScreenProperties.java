package me.mchacker1706.game.properties;

public class ScreenProperties {

    public static final int widthWindowed = 800;
    public static final int heightWindowed = 600;

    public static int width = widthWindowed;
    public static int height = heightWindowed;


    public static int screenModifier = FinalProperties.displayMode.equals(FinalProperties.DisplayMode.FULLSCREEN)||FinalProperties.displayMode.equals(FinalProperties.DisplayMode.FULLSCREEN_BORDERLESS)?2:1;

        public static int FPSLimit = -1;


}
