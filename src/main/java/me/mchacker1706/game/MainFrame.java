package me.mchacker1706.game;

import me.mchacker1706.game.entity.Entity;
import me.mchacker1706.game.properties.FinalProperties;
import me.mchacker1706.game.properties.ScreenProperties;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.util.ArrayList;


public class MainFrame extends JFrame{

    private BufferStrategy strategy;

    public MainFrame()
    {

        refreshSettings();

    }


    public void refreshSettings()
    {

        setBackground(Color.BLACK);
            setVisible(false);
            setUndecorated(false);
            ScreenProperties.height = ScreenProperties.heightWindowed;
            ScreenProperties.width = ScreenProperties.widthWindowed;


        GraphicsDevice device = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();

        if(FinalProperties.displayMode.equals(FinalProperties.DisplayMode.FULLSCREEN) || FinalProperties.displayMode.equals(FinalProperties.DisplayMode.FULLSCREEN_BORDERLESS))
        {
            ScreenProperties.width = device.getDisplayMode().getWidth();
            ScreenProperties.height = device.getDisplayMode().getHeight();
        }

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(ScreenProperties.width, ScreenProperties.height);
        setTitle(FinalProperties.windowName);
        setResizable(false);
        setCursor(Toolkit.getDefaultToolkit().createCustomCursor(new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB), new Point(0, 0), "blank cursor"));
        try {
            setIconImage(ImageIO.read(getClass().getResourceAsStream("gfx/icon.png")));
        }catch (Exception e){}
        if(!FinalProperties.displayMode.equals(FinalProperties.DisplayMode.NORMAL))
            setUndecorated(true);

        if(!FinalProperties.displayMode.equals(FinalProperties.DisplayMode.FULLSCREEN)) {
            setVisible(true);
            setLocationRelativeTo(null);
        }

       if(FinalProperties.displayMode.equals(FinalProperties.DisplayMode.FULLSCREEN))
           device.setFullScreenWindow(this);



        createBufferStrategy(2);

        strategy = getBufferStrategy();





    }

    public void tick()
    {

        repaintScreen();
    }


    public void repaintScreen(){

        try {
            Graphics g = strategy.getDrawGraphics();
            g.clearRect(0, 0, ScreenProperties.width, ScreenProperties.height);

            drawAll(g);


            strategy.show();

            g.dispose();
        }catch (Exception e){}
    }


    public void drawAll(Graphics g) throws Exception
    {


        Graphics2D g2d = (Graphics2D) g;

        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        MainThread.getBackground().paint(g);

        try {
            for (Entity e : ((ArrayList<Entity>) Entity.getEntities().clone())) {

                if (e.equals(MainThread.getEntityPlayer()))
                    continue;

                e.paint(g);
            }
        }catch (Exception e){}


        MainThread.getEntityPlayer().paint(g);

        g.setColor(Color.YELLOW);

        if(MainThread.isFpsActive()){
                Font font = Font.createFont(Font.PLAIN, getClass().getClassLoader().getResourceAsStream("font/game_over.ttf")).deriveFont(80f);


                drawTxt(g, MainThread.getLastFPS() + " FPS " + MainThread.getLastTPS() + " TPS", 0, ScreenProperties.height - MainThread.getMainFrame().getInsets().top, font);

        }





        if(GameState.getState().equals(GameState.INGAME)) {


            Font font = Font.createFont(Font.PLAIN, getClass().getClassLoader().getResourceAsStream("font/game_over.ttf")).deriveFont(80f);


            FontMetrics lm = g.getFontMetrics(font);

            String score = "Points: " + MainThread.getPoints();



            drawTxt(g, score, (int) (ScreenProperties.width - MainThread.getMainFrame().getInsets().left - lm.getFont().getStringBounds(score, lm.getFontRenderContext()).getWidth()) - 10, ScreenProperties.height - MainThread.getMainFrame().getInsets().top, font);

        }
        if(GameState.getState().equals(GameState.GAME_OVER)){


            g2d.setComposite(AlphaComposite.getInstance(
                    AlphaComposite.SRC_OVER, 0.7f));

            Font font = Font.createFont(Font.PLAIN, getClass().getClassLoader().getResourceAsStream("font/game_over.ttf")).deriveFont(150f);

            FontMetrics lm = g.getFontMetrics(font);


            String s = "Game Over!";
            String s2 = "Score: " + MainThread.getPoints();
            String s3 = "Press Space to restart!";

            if(t2)
                alpha2 += 0.01f;
            else
                alpha2 -= 0.01f;

            if (alpha2 >= 0.7f) {
                alpha2 = 0.7f;
                t2= false;
            }else if(alpha2<0.05f){
                alpha2 = 0.05f;
                t2 = true;
            }


            int height = (int)lm.getFont().getStringBounds(s2, lm.getFontRenderContext()).getHeight();

            drawTxt(g, s2,
                    (int) ((ScreenProperties.width - MainThread.getMainFrame().getInsets().left)/2- lm.getFont().getStringBounds(s2, lm.getFontRenderContext()).getWidth()/2),
                    ScreenProperties.height/2 - getInsets().top,
                    font
            );



            font = Font.createFont(Font.PLAIN, getClass().getClassLoader().getResourceAsStream("font/game_over.ttf")).deriveFont(100f);
            lm = g.getFontMetrics(font);


            drawTxt(g, s, (int) ((ScreenProperties.width - getInsets().left - lm.getFont().getStringBounds(s, lm.getFontRenderContext()).getWidth()) / 2),
                    ScreenProperties.height / 2 - getInsets().top-height+10,
                    font);


            font = Font.createFont(Font.PLAIN, getClass().getClassLoader().getResourceAsStream("font/game_over.ttf")).deriveFont(150f);
            lm = g.getFontMetrics(font);

            g2d.setComposite(AlphaComposite.getInstance(
                    AlphaComposite.SRC_OVER, alpha2));

            drawTxt(g, s3,
                    (int) ((ScreenProperties.width - MainThread.getMainFrame().getInsets().left) / 2 - lm.getFont().getStringBounds(s3, lm.getFontRenderContext()).getWidth() / 2),
                    (ScreenProperties.height / 2 - MainThread.getMainFrame().getInsets().top + (int) lm.getFont().getStringBounds(s3, lm.getFontRenderContext()).getHeight()) - 10,
                    font
            );


            g2d.setComposite(AlphaComposite.getInstance(
                    AlphaComposite.SRC_OVER, 1f));

        }else{
            alpha2 = 0;
        }
        if(GameState.getState().equals(GameState.LOBBY)){


            //do the drawing here

            //increase the opacity and repaint
            if(t)
            alpha += 0.01f;
            else
            alpha -= 0.01f;

            if (alpha >= 0.7f) {
                alpha = 0.7f;
                t= false;
            }else if(alpha<0.05f){
                alpha = 0.05f;
                t=true;
            }

            String s = "Ez Game";
            String s2 = "Press Space to start!";



            g2d.setComposite(AlphaComposite.getInstance(
                    AlphaComposite.SRC_OVER, 0.7f));


            Font font = Font.createFont(Font.PLAIN, getClass().getClassLoader().getResourceAsStream("font/game_over.ttf")).deriveFont(200f);

            FontMetrics lm = g.getFontMetrics(font);
            drawTxt(g, s, (int) ((ScreenProperties.width - MainThread.getMainFrame().getInsets().left - lm.getFont().getStringBounds(s, lm.getFontRenderContext()).getWidth()) / 2), ScreenProperties.height / 2 - MainThread.getMainFrame().getInsets().top, font);



            font = Font.createFont(Font.PLAIN, getClass().getClassLoader().getResourceAsStream("font/game_over.ttf")).deriveFont(150f);
            lm = g.getFontMetrics(font);



            //set the opacity
            g2d.setComposite(AlphaComposite.getInstance(
                    AlphaComposite.SRC_OVER, alpha));


            drawTxt(g, s2,
                    (int) ((ScreenProperties.width - MainThread.getMainFrame().getInsets().left - lm.getFont().getStringBounds(s2, lm.getFontRenderContext()).getWidth()) / 2),
                    ((ScreenProperties.height / 2 - MainThread.getMainFrame().getInsets().top + (int) lm.getFont().getStringBounds(s2, lm.getFontRenderContext()).getHeight())),
                    font
            );

            g2d.setComposite(AlphaComposite.getInstance(
                    AlphaComposite.SRC_OVER, 1f));



        }else{
            alpha = 0f;
        }

        if(!GameState.getState().equals(GameState.LOBBY)){
            Font font = Font.createFont(Font.PLAIN, getClass().getClassLoader().getResourceAsStream("font/game_over.ttf")).deriveFont(80f);

            g2d.setStroke(new BasicStroke(3));
            FontMetrics lm = g.getFontMetrics(font);
            String s = "Esc";
            g.drawRect(-3, ((int) lm.getFont().getStringBounds(s, lm.getFontRenderContext()).getHeight() - 11) - (int) lm.getFont().getStringBounds(s, lm.getFontRenderContext()).getHeight(), (int) lm.getFont().getStringBounds(s, lm.getFontRenderContext()).getWidth()+6, (int) lm.getFont().getStringBounds(s, lm.getFontRenderContext()).getHeight());

            drawTxt(g, s, 0, (int) (lm.getFont().getStringBounds(s, lm.getFontRenderContext()).getHeight() - 17), font);


        }

    }
    float alpha = 0.0f;
    float alpha2 = 0.0f;
    boolean t = true;
    boolean t2 = true;


    private void drawTxt(Graphics g, String txt, int x, int y,Font f)
    {

        Graphics2D g2d = (Graphics2D) g;
        Color c = g.getColor();

        Composite composite = g2d.getComposite();
        g.setFont(f);
        g.drawString(txt, x, y);


        if(MainThread.isHitboxActive()) {


            g2d.setComposite(AlphaComposite.getInstance(
                    AlphaComposite.SRC_OVER, 1f));


            FontMetrics lm = g.getFontMetrics(f);

            g.setColor(Color.RED);
            g.drawRect(x, y - (int) lm.getFont().getStringBounds(txt, lm.getFontRenderContext()).getHeight(), (int) lm.getFont().getStringBounds(txt, lm.getFontRenderContext()).getWidth(), (int) lm.getFont().getStringBounds(txt, lm.getFontRenderContext()).getHeight());
        }

        ((Graphics2D) g).setComposite(composite);
        g.setColor(c);
    }








}
