package me.mchacker1706.game.entity;

import lombok.Getter;
import lombok.Setter;
import me.mchacker1706.game.Location;

import java.awt.*;


public abstract class EntityLiving extends Entity{

    @Getter
    private float maxHealth;

    @Getter @Setter
    private float health;


    public EntityLiving(int maxHealth){
        this.maxHealth = maxHealth;
        this.health = maxHealth;
    }

    public EntityLiving(float speed, Location location, int maxHealth)
    {
        super(speed, location);

        this.maxHealth = maxHealth;
        this.health = maxHealth;

    }


    public boolean hasMaxHealth(){
        return maxHealth == health;
    }

    public void damage(float damage){
        this.health-=damage;
        if(health<=0)
            onDeath();
    }

    public void heal(float damage){
        this.health+=damage;
    }


    public abstract void onDeath();

    public void drawHealth(Graphics g){

        if(hasMaxHealth())
            return;

        g.setColor(Color.GRAY);
        g.drawRect((int) getLocation().getPosX(), (int) getLocation().getPosY() + 5 + getHeight(), getWidth(), 5);

        if(getHealth() <= 0)
            return;


        g.setColor(getHealth() <= 25 ? Color.RED : getHealth() <= 50 ? Color.YELLOW : Color.GREEN);
        g.fillRect((int) getLocation().getPosX(), (int) getLocation().getPosY() + 5 + getHeight(), getWidth() - map((int)getHealth(), (int)getMaxHealth(), 0, 0, getWidth()), 5);

    }

    private int map(int x, int in_min, int in_max, int out_min, int out_max)
    {
        return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }

}
