package me.mchacker1706.game.entity;

import me.mchacker1706.game.GameState;
import me.mchacker1706.game.MainThread;
import me.mchacker1706.game.SoundUtils;
import me.mchacker1706.game.properties.ScreenProperties;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class EntityItem extends Entity{


    private static BufferedImage texture;

    static {
        try{

            texture = ImageIO.read(EntityItem.class.getClassLoader().getResourceAsStream("gfx/coin_converted (1).png"));

        }catch (Exception e){}
    }


    public EntityItem()
    {


        getLocation().setPosX(ScreenProperties.width - getWidth() - MainThread.getMainFrame().getInsets().left);


        setHeight(15*ScreenProperties.screenModifier);
        setWidth(15*ScreenProperties.screenModifier);
        setCanCrossOthers(true);
    }


    @Override
    public void tick()
    {


        if(!GameState.getState().equals(GameState.INGAME))
            return;


        if(getLocation().getPosX()<=0) {
            destory();
        }

        if(isColliding(MainThread.getEntityPlayer()))
        {
            MainThread.addCoin();
            SoundUtils.playSound("coin");
            destory();
        }


    }

    @Override
    public void paint(Graphics g)
    {

        if(!GameState.getState().equals(GameState.INGAME))
            return;



        if(MainThread.isHitboxActive()) {
            g.setColor(Color.RED);
            g.drawRect((int) getLocation().getPosX(), (int) getLocation().getPosY(), getWidth(), getHeight());
        }

        for(Entity e : (ArrayList<Entity>)Entity.getEntities().clone()) {

            if (e.equals(this) || !(e instanceof EntityLiving))
                continue;

            if (e.getAsRectangle().intersects(getAsRectangle()))
                return;
        }

        g.drawImage(texture, (int)getLocation().getPosX(), (int)getLocation().getPosY(), getWidth(), getHeight(),  null);



    }

    @Override
    public void handleMovement()
    {


        if(GameState.getState().equals(GameState.GAME_OVER))
            return;

        setSpeedX(MainThread.getBackground().getSpeed());

        moveNegX();


    }
}
