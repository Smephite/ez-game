package me.mchacker1706.game.entity;

import me.mchacker1706.game.AnimationHelper;
import me.mchacker1706.game.GameState;
import me.mchacker1706.game.Location;
import me.mchacker1706.game.MainThread;
import me.mchacker1706.game.properties.ScreenProperties;

import java.awt.*;
import java.util.Random;

public class EntityEnemy extends EntityLiving{


    private long spawn = -1;
    private static AnimationHelper animationHelper = new AnimationHelper("gfx/enemy1.png", "gfx/enemy2.png", "gfx/enemy3.png", "gfx/enemy2.png");
    public EntityEnemy()
    {



        super(100);

        spawn = MainThread.getTick();

        setSpeedX(5);
        setSpeedY(5);
        setHeight(50);
        setWidth(50);
        setCanCrossOthers(true);
        nextShoot = MainThread.getTick()+100;
        nextAnimation = MainThread.getTick()+50;

    }


    @Override
    public void onDeath()
    {

        destory();
        animationHelper.reset();
        MainThread.addPoint(25);
        MainThread.setEnemy(null);

    }

    private long nextAnimation = 0;

    @Override
    public void tick()
    {

        if(!GameState.getState().equals(GameState.INGAME))
            return;


        if(spawn!= -1 && MainThread.getTick()-spawn>50) {

            for(Entity e : Entity.getEntities())
                if(!e.getAsRectangle().intersects(getAsRectangle()) || e.isCanCrossOthers()) {

                    setCanCrossOthers(false);
                    spawn = -1;
                    break;
                }

        }

        setSpeedX(MainThread.getEntityPlayer().getSpeedX());
        setSpeedY(MainThread.getEntityPlayer().getSpeedY());

        if(nextShoot<=MainThread.getTick()){

            nextShoot = MainThread.getTick()+250;
            new EntityBullet(this, MainThread.getEntityPlayer().getLocation().clone().add(MainThread.getEntityPlayer().getWidth()/2, MainThread.getEntityPlayer().getHeight()/2), MainThread.getEntityPlayer().getLocation().getPosX()>getLocation().getPosX()?3:-3, 0);

        }

        if(nextAnimation<=MainThread.getTick()){
            nextAnimation = MainThread.getTick()+50;
            animationHelper.next();
        }

    }

    long nextShoot = 0;

    @Override
    public void paint(Graphics g)
    {

        if(!GameState.getState().equals(GameState.INGAME))
            return;

        g.drawImage(animationHelper.getCurrent(), (int)getLocation().getPosX(), (int)getLocation().getPosY(), getWidth(), getHeight(), null);
        drawHealth(g);

        if(MainThread.isHitboxActive()) {

            g.setColor(Color.RED);


            g.fillRect(nextX, nextY, 5, 5);
            g.drawRect((int) getLocation().getPosX(), (int) getLocation().getPosY(), getWidth(), getHeight());
        }


    }



    int nextY = -1;
    int nextX = -1;
    Random r = new Random();
    long nextL = 0;

    @Override
    public void handleMovement()
    {

        if(!GameState.getState().equals(GameState.INGAME))
            return;


        EntityPlayer player = MainThread.getEntityPlayer();


        if(nextL<=MainThread.getTick() && (hasReached() || nextY<0 || nextX < 0 || nextX > ScreenProperties.width-getWidth()-MainThread.getMainFrame().getInsets().left || nextY > ScreenProperties.height-getHeight()-MainThread.getMainFrame().getInsets().top|| player.getLocation().distance(getLocation()) >=300 || player.getLocation().distance(getLocation())<=100)) {
            nextY = r.nextInt(500)-250 +(int)player.getLocation().getPosY()+player.getHeight()/2;
            nextX = r.nextInt(500)-250+(int)player.getLocation().getPosX()+player.getWidth()/2;
            nextL=MainThread.getTick()+10;

        }

        if(nextY> getLocation().getPosY())
            movePosY();
        if(nextY<getLocation().getPosY())
            moveNegY();
        if(nextX>getLocation().getPosX())
            movePosX();
        if(nextX<getLocation().getPosX())
            moveNegX();


    }



    private boolean hasReached(){

        return new Location(nextX, nextY).distance(getLocation()) <=getSpeedY()*2;

    }





}
