package me.mchacker1706.game.entity;

import me.mchacker1706.game.*;
import me.mchacker1706.game.properties.ScreenProperties;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class EntityBullet extends Entity{

    private static BufferedImage image;

    static
    {

        try {
            image = ImageIO.read(EntityBullet.class.getClassLoader().getResourceAsStream("gfx/schuss.png"));
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    Location startingLocation;
    Location targetLocation;
    EntityLiving parent;

    public EntityBullet(EntityLiving father, Location pointOnLine, float speedConstanteX, float speedConstanteY){

        setWidth(image.getWidth() * ScreenProperties.screenModifier);
        setHeight(image.getHeight() * ScreenProperties.screenModifier);
        setCanCrossOthers(true);
        this.parent = father;



        setLocation(new Location(father.getLocation().getPosX() + father.getWidth()/2, father.getLocation().getPosY() + father.getHeight() / 2 - getHeight() / 2));

        setCanLeaveScreen(true);

        SoundUtils.playSound("laser");


        startingLocation = getLocation().clone();
        this.targetLocation = pointOnLine;


        setSpeedX((targetLocation.getPosX()-startingLocation.getPosX())/((targetLocation.getPosX()-startingLocation.getPosX())/speedConstanteX));
        setSpeedY((targetLocation.getPosY() - startingLocation.getPosY()) / ((targetLocation.getPosY() - startingLocation.getPosY()) / speedConstanteY));

        System.out.println(getSpeedX());
        System.out.println(getSpeedY());

    }


    public EntityBullet(EntityLiving father, Location pointOnLine, float speedConstante)
    {
        this(father, pointOnLine, speedConstante, speedConstante);
    }



    @Override
    public void handleMovement()
    {

        if(GameState.getState().equals(GameState.GAME_OVER))
            return;




        movePosX();
        movePosY();

    }

    @Override
    public void tick()
    {

        if(!GameState.getState().equals(GameState.INGAME))
            return;


        for(Entity e : (ArrayList<Entity>)Entity.getEntities().clone()) {

            if(e.equals(this))
                continue;

            if (e.getAsRectangle().intersects(getAsRectangle()))
                if (e instanceof EntityLiving) {
                    {

                        if(e.equals(parent))
                            continue;
                        //TODO remove
                        destory();
                        ((EntityLiving) e).damage(25f);
                    }
                }
        }



        if(!isInScreen())
            destory();


    }

    @Override
    public void paint(Graphics g)
    {

        if(!GameState.getState().equals(GameState.INGAME))
            return;

        if(MainThread.isHitboxActive()) {
            g.setColor(Color.RED);
            g.drawRect((int) getLocation().getPosX(), (int) getLocation().getPosY(), getWidth(), getHeight());
            g.drawLine((int) startingLocation.getPosX(), (int) startingLocation.getPosY(), (int) targetLocation.getPosX(), (int) targetLocation.getPosY());
        }

        for(Entity e : (ArrayList<Entity>)Entity.getEntities().clone()) {

            if (e.equals(this) || !(e instanceof EntityLiving))
                continue;

            if (e.getAsRectangle().intersects(getAsRectangle()))
                return;

        }

        g.drawImage(ImageHelper.rotate(image, 0), (int) getLocation().getPosX(), (int) getLocation().getPosY(), getWidth(), getHeight(), null);
        //g.setColor(Color.CYAN);
          //  g.fillRect((int)getLocation().getPosX(), (int)getLocation().getPosY(), getWidth(), getHeight());


    }




}
