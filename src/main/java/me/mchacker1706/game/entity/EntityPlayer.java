package me.mchacker1706.game.entity;

import lombok.Setter;
import me.mchacker1706.game.GameState;
import me.mchacker1706.game.KeyHandler;
import me.mchacker1706.game.Location;
import me.mchacker1706.game.MainThread;
import me.mchacker1706.game.enums.Key;
import me.mchacker1706.game.properties.ScreenProperties;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;


public class EntityPlayer extends EntityLiving{


    private BufferedImage texture;

    public EntityPlayer()
    {
        super(4, new Location(50, 300), 100);
        try {
            texture = ImageIO.read(getClass().getClassLoader().getResourceAsStream("gfx/raumschiffchen.png"));
        }catch (Exception e){
            e.printStackTrace();
            System.exit(-2);
        }

        setHeight(texture.getHeight()*ScreenProperties.screenModifier);
        setWidth(texture.getWidth()*ScreenProperties.screenModifier);
        setSpeedX(3);

    }

    @Setter
    private long nextShoot = 0;


    @Override
    public void onDeath()
    {

        GameState.setState(GameState.GAME_OVER);
        KeyHandler.getPressedKeys().put(Key.KEY_SHOOT, false);
        MainThread.setEnemy(null);

    }

    @Override
    public void tick()
    {


        if(!GameState.getState().equals(GameState.INGAME))
            return;


        //damage(0.5f);


            float speed = map((int) getLocation().getPosX(), 0, ScreenProperties.width - getWidth() - MainThread.getMainFrame().getInsets().left - 5, 2000, 1) / 1000f;

            float speedBG = map((int)getLocation().getPosX(), 0, ScreenProperties.width - getWidth() - MainThread.getMainFrame().getInsets().left - 5, 1, 3000) / 1000f;

            MainThread.getBackground().setSpeed(3 + speedBG);

        if(mayMove(getLocation().getPosX() - getSpeedX() + speed, getLocation().getPosY()))
            getLocation().setPosX(getLocation().getPosX() - getSpeedX() + speed);


            if(KeyHandler.getInstance().isKeyPressed(Key.KEY_SHOOT))
            {

                if(nextShoot<=MainThread.getTick()){
                    nextShoot=MainThread.getTick()+50;
                    new EntityBullet(this, new Location(getLocation().getPosX()+10000, getLocation().getPosY()), 0);
                }

            }


    }

    @Override
    public void handleMovement()
    {


        if(!GameState.getState().equals(GameState.INGAME))
            return;


        if(KeyHandler.getInstance().isKeyPressed(Key.KEY_UP))
            moveNegY();
        if(KeyHandler.getInstance().isKeyPressed(Key.KEY_DOWN))
            movePosY();
        if(KeyHandler.getInstance().isKeyPressed(Key.KEY_RIGHT))
            movePosX();
        else
            if(mayMove(getLocation().getPosX() - (map((int) getLocation().getPosX(), 0, ScreenProperties.width - getWidth() - MainThread.getMainFrame().getInsets().left - 5, 500, 3000) / 1000f), getLocation().getPosY()))
                getLocation().setPosX(getLocation().getPosX() - (map((int) getLocation().getPosX(), 0, ScreenProperties.width - getWidth() - MainThread.getMainFrame().getInsets().left - 5, 500, 3000) / 1000f));


    }

    @Override
    public void paint(Graphics g) {

        if(!GameState.getState().equals(GameState.INGAME))
            return;

        drawHealth(g);

        g.drawImage(texture, (int)getLocation().getPosX(), (int)getLocation().getPosY(), getWidth(), getHeight(), null);
        if(MainThread.isHitboxActive()) {
            g.setColor(Color.RED);
            g.drawRect((int) getLocation().getPosX(), (int) getLocation().getPosY(), getWidth(), getHeight());
        }

    }


    private int map(int x, int in_min, int in_max, int out_min, int out_max)
    {
        return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }





    @Override
    public void handleBorder() {
        if(isCanLeaveScreen())
            return;

        if(getLocation().getPosY()<0) getLocation().setPosY(0);
        if(getLocation().getPosX()<0) getLocation().setPosX(0);
        if(getLocation().getPosY()>ScreenProperties.height-getWidth()-MainThread.getMainFrame().getInsets().top-10)
            getLocation().setPosY(ScreenProperties.height - getHeight() - MainThread.getMainFrame().getInsets().top - 10);
        if(getLocation().getPosX()>ScreenProperties.width-getWidth()-MainThread.getMainFrame().getInsets().left)
            getLocation().setPosX(ScreenProperties.width - getWidth() - MainThread.getMainFrame().getInsets().left);

    }
}
