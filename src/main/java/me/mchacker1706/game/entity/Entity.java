package me.mchacker1706.game.entity;

import lombok.Getter;
import lombok.Setter;
import me.mchacker1706.game.Location;
import me.mchacker1706.game.MainThread;
import me.mchacker1706.game.properties.ScreenProperties;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.UUID;

public abstract class Entity {

    @Getter
    private static final ArrayList<Entity> entities = new ArrayList<>();

    @Getter @Setter
    private float speedX, speedY; //pixels per tick

    @Getter @Setter
    private Location location;

    @Getter
    private String name;

    @Getter
    private UUID uuid;

    @Getter
    private int id;

    @Getter @Setter
    private int height, width;

    @Getter @Setter
    private boolean canCrossOthers = false;

    @Getter @Setter
    private boolean canLeaveScreen = false;

    public Entity(float speedX, float speedY, Location location, String name, UUID uuid, int height, int width)
    {
        id=entities.size();
        entities.add(this);
        this.speedX = speedX;
        this.speedY = speedY;
        this.name = name;
        this.uuid = uuid;
        this.location = location;
    }


    public Entity()
    {

        this(5, 5, new Location(300, 300), "Entity:" + entities.size(), UUID.randomUUID(), 50, 50);

    }

    public Entity(float speed, Location location)
    {

        this(speed, speed, location, "Entity:"+entities.size(), UUID.randomUUID(), 50, 50);

    }

    public Entity(float speed, Location location, int width, int height)
    {

        this(speed, speed, location, "Entity:"+entities.size(), UUID.randomUUID(), width, height);

    }

    public Entity(float speed, Location location, String name)
    {

        this(speed, speed, location, name, UUID.randomUUID(), 50, 50);

    }

    public Entity(float speed, Location location, String name, UUID uuid)
    {

        this(speed, speed, location, name, uuid, 50, 50);

    }

    public void handle(){
        handleMovement();
        handleBorder();
        tick();
    }


    public abstract void tick();
    public abstract void paint(Graphics g);
    public abstract void handleMovement();

    public void movePosX(){ if(mayMove((int)(location.getPosX()+speedX), (int)location.getPosY())) location.addX(speedX); }
    public void moveNegX(){ if(mayMove((int)(location.getPosX()-speedX), (int)location.getPosY())) location.addX(-speedX); }
    public void movePosY(){ if(mayMove(location.getPosX(), (int)(getLocation().getPosY()+speedY))) location.addY(speedY); }
    public void moveNegY(){ if(mayMove((int)location.getPosX(), (int)(location.getPosY()-speedY))) location.addY(-speedY); }

    public boolean mayMove(float x, float y)
    {

    if(canCrossOthers)
        return true;

        Rectangle r = new Rectangle((int)x, (int)y, width, height);


        for(Entity e : Entity.entities) {
            if(e.equals(this))
                continue;

            if (e.getAsRectangle().intersects(r) && !e.canCrossOthers)
                return false;
        }

        return true;

    }

    public void handleBorder()
    {

        if(canLeaveScreen)
            return;

        if(location.getPosY()<0)location.setPosY(0);
        if(location.getPosX()<0)location.setPosX(0);
        if(location.getPosY()>ScreenProperties.height-width-MainThread.getMainFrame().getInsets().top)location.setPosY(ScreenProperties.height-height-MainThread.getMainFrame().getInsets().top);
        if(location.getPosX()>ScreenProperties.width-width-MainThread.getMainFrame().getInsets().left)location.setPosX(ScreenProperties.width-width-MainThread.getMainFrame().getInsets().left);

    }



    public boolean isInScreen()
    {

        return location.getPosY()>0&&
                location.getPosX()>0&&
                    location.getPosY()<ScreenProperties.height-width-MainThread.getMainFrame().getInsets().top&&
                        location.getPosX()<ScreenProperties.width-width-MainThread.getMainFrame().getInsets().left;


    }

    public Rectangle getAsRectangle(){
        return new Rectangle((int)location.getPosX(), (int)location.getPosY(), width, height);
    }

    public boolean isColliding(Entity e)
    {
        return getAsRectangle().intersects(e.getAsRectangle());
    }

    public boolean isInRectange(Rectangle other){
        return other.contains(getAsRectangle());
    }

    public boolean containsRectange(Rectangle other){
        return getAsRectangle().contains(other);
    }


    public void destory(){
        entities.remove(this);
    }
}
