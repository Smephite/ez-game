package me.mchacker1706.game;

import lombok.Getter;
import me.mchacker1706.game.enums.Key;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;

public class KeyHandler implements java.awt.event.KeyListener {

    @Getter
    private static KeyHandler instance;
    @Getter
    private static HashMap<Key, Boolean> pressedKeys = new HashMap<>();

    private ArrayList<Key> hardPressedKeys = new ArrayList<>();

    public KeyHandler(){
        if(instance!=null)
            return;
        instance = this;
    }

    @Override
    public void keyTyped(KeyEvent e) {}


    @Override
    public void keyPressed(KeyEvent e)
    {
        Key k = Key.byEvent(e.getKeyCode());

        if(hardPressedKeys.contains(k))
            return;

        hardPressedKeys.add(k);


        if(k == null)
            return;

        pressedKeys.put(k, true);


    }

    @Override
    public void keyReleased(KeyEvent e)
    {

        Key k = Key.byEvent(e.getKeyCode());
        try {
            if (hardPressedKeys.contains(k))
                hardPressedKeys.remove(k);
        }catch (Exception ee){
            ee.printStackTrace();
        }


        if(k == null)
            return;

        pressedKeys.put(k, false);
    }


    public boolean isKeyPressed(Key k){


        if(pressedKeys.containsKey(k)) {
            return pressedKeys.get(k);
        }
        else
            return false;

    }

}
